import React from "react";

const Dashboard = () => (
  <div className="w-75 m-auto">
    <h4>Basic Password Form</h4>
    <hr />
    <form className="mt-3">
      <div className="mb-3">
        <label htmlFor="exampleInputEmail1" className="form-label">
          Email address
        </label>
        <input
          type="email"
          className="form-control"
          id="exampleInputEmail1"
          aria-describedby="emailHelp"
        />
        <div id="emailHelp" className="form-text">
          We'll never share your email with anyone else.
        </div>
      </div>
      <div className="mb-3">
        <label htmlFor="exampleInputPassword1" className="form-label">
          Password
        </label>
        <input
          type="password"
          className="form-control"
          id="exampleInputPassword1"
        />
      </div>
      <div className="mb-3 form-check">
        <input
          type="checkbox"
          className="form-check-input  pt-2"
          id="exampleCheck1"
          style={{ width: "20px", height: "20px" }}
        />
        <label className="form-check-label ms-2" htmlFor="exampleCheck1">
          Keep me login
        </label>
      </div>

      <button type="submit" className="btn btn-primary text-white px-3">
        Submit
      </button>
    </form>
  </div>
);

export default Dashboard;
