import React from "react";
import Select from "react-select";

const options = [
  { value: "nepal", label: "Nepal" },
  { value: "USA", label: "USA" },
  { value: "spain", label: "spain" },
];

const ReactSelect = () => <Select options={options} />;

export default ReactSelect;
