import React from "react";

import Layout from "containers/Layout/Layout";
import ReactSelect from "components/Home/ReactSelect";
import CustomDatePicker from "components/Home/DatePicker";

const Home = () => {
  return (
    <Layout>
      <section>
        <div className="w-75 m-auto">
          <h4>Basic Form</h4>
          <hr />
          <form className="mt-3">
            <div className="mb-3">
              <label htmlFor="exampleInputName1" className="form-label">
                Full Name
              </label>
              <input
                type="text"
                className="form-control"
                id="exampleInputName1"
                aria-describedby="nameHelp"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleInputPhoneNumber1" className="form-label">
                Phone Number
              </label>
              <input
                type="number"
                className="form-control"
                id="exampleInputPhoneNumber1"
                aria-describedby="phoneNumberHelper"
              />
            </div>
            <div className="mb-3">
              <label htmlFor="exampleInputEmail1" className="form-label">
                Email address
              </label>
              <input
                type="email"
                className="form-control"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
              />
              <div id="emailHelp" className="form-text">
                We'll never share your email with anyone else.
              </div>
            </div>
            <div className="mb-3">
              <label htmlFor="exampleInputPassword1" className="form-label">
                Password
              </label>
              <input
                type="password"
                className="form-control"
                id="exampleInputPassword1"
              />
            </div>
            <div className="mb-3">
              <ReactSelect />
            </div>
            <div>
              <CustomDatePicker />
            </div>
            <div className="mb-3 form-check mt-2">
              <input
                type="checkbox"
                className="form-check-input  pt-2"
                id="exampleCheck1"
                style={{ width: "20px", height: "20px" }}
              />
              <label className="form-check-label ms-2" htmlFor="exampleCheck1">
                Keep me login
              </label>
            </div>
            <div className="mb-3 form-check">
              <input
                type="radio"
                className="form-check-input  pt-2"
                id="exampleCheck2"
                style={{ width: "20px", height: "20px" }}
              />
              <label className="form-check-label ms-2" htmlFor="exampleCheck2">
                keep my name
              </label>
            </div>
            <button type="submit" className="btn btn-primary text-white px-3">
              Submit
            </button>
          </form>
        </div>
      </section>
    </Layout>
  );
};

export default Home;
